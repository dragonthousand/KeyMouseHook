﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using static System.Net.Mime.MediaTypeNames;
using static System.Runtime.CompilerServices.RuntimeHelpers;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;
using System.IO;
using System.Reflection;
using WinScanInputSimulate;

namespace WinScanInputSimulate35
{
    public partial class FormMain : Form
    {
        [DllImport("user32.dll")]
        static extern short VkKeyScan(char ch);

        [DllImport("user32.dll")]
        public static extern void SwitchToThisWindow(IntPtr hWnd, bool fAltTab);

        [DllImport("user32.dll")]
        private static extern bool SetForegroundWindow(IntPtr hWnd);
        [DllImport("user32.dll")]
        private static extern IntPtr GetForegroundWindow();
        [DllImport("user32.dll")]
        private static extern bool IsIconic(IntPtr hWnd);

        [DllImport("user32.dll")]
        private static extern bool ShowWindow(IntPtr hWnd, int nCmdShow); 
        [DllImport("user32.dll")]
        private static extern bool SetCursorPos(int X, int Y);

        #region key
        [DllImport("user32.dll", SetLastError = true)]
        public static extern void keybd_event(byte bVk, byte bScan, int dwFlags, int dwExtraInfo);

        public const byte VK_UP = 0x26;
        public const byte VK_DOWN = 0x28;
        public const byte VK_LEFT = 0x25;
        public const byte VK_RIGHT = 0x27;

        public const byte VK_BackSpace = 0x8;

        public const byte VK_Ctrl = 0x11;//Ctrl的虚拟键码
        public const byte VK_A = 0x41;//A键的虚拟键码

        public const int KEYEVENTF_EXTENDEDKEY = 0x1;
        public const int KEYEVENTF_KEYUP = 0x2;

        #endregion

        // SW_SHOWMAXIMIZED宏定义，用于检查窗口是否最大化
        private const int SW_SHOWMAXIMIZED = 3;

        #region mouse click

        [DllImport("user32.dll")]
        private static extern void mouse_event(uint dwFlags, int dx, int dy, int cButtons, uint dwData);

        private const uint MOUSEEVENTF_LEFTDOWN = 0x02; // 鼠标左键按下
        private const uint MOUSEEVENTF_LEFTUP = 0x04;   // 鼠标左键抬起


        [System.Runtime.InteropServices.DllImport("user32")]
        private static extern int mouse_event(int dwFlags, int dx, int dy, int cButtons, int dwExtraInfo);
        //移动鼠标 
        const int MOUSEEVENTF_MOVE = 0x0001;
        ////模拟鼠标左键按下 
        //const int MOUSEEVENTF_LEFTDOWN = 0x0002;
        ////模拟鼠标左键抬起 
        //const int MOUSEEVENTF_LEFTUP = 0x0004;
        //模拟鼠标右键按下 
        const int MOUSEEVENTF_RIGHTDOWN = 0x0008;
        //模拟鼠标右键抬起 
        const int MOUSEEVENTF_RIGHTUP = 0x0010;
        //模拟鼠标中键按下 
        const int MOUSEEVENTF_MIDDLEDOWN = 0x0020;
        //模拟鼠标中键抬起 
        const int MOUSEEVENTF_MIDDLEUP = 0x0040;
        //标示是否采用绝对坐标 
        const int MOUSEEVENTF_ABSOLUTE = 0x8000;


        #endregion

        PlcTcpIpHelper tcpIpHelper; 

        #region 属性变量

        // 模拟鼠标点击的坐标
        int _mouseClickX = 100; // X坐标
        int _mouseClickY = 200; // Y坐标

        //最小化托盘
        private NotifyIcon notifyIcon;

        private const string SECTIONKEY = "base";
        private const string PARAMFILENAME = "param.ini";

        private string paramFile;

        private string serviceIp;
        private int port;
        private string softwareName;

        private int ThreadSleep = 2000;
        private int TextEntrySleep = 500;
        private int ShowItemCount = 200;
        private int TotalItemCount = 0;
         
        private AsyncTcpClient scanCameraClient;

        private int IsMinToNotifyIcon;
        private bool isStart = false;
        private bool isDebug = false;

        //条码写入到指定目录下，如果没有目录则输入到焦点下
        private string WriteFilePath = string.Empty;
        private bool isWirteFile;
        private bool IsFocusInput;
        private bool IsStartProcesses;
        private bool IsSpecifyFocusProcess;//是否明确指定焦点输入软件

        /// <summary>
        /// 要启动的进程名称，可以在任务管理器里查看，一般是不带.exe后缀的;
        /// </summary>
        private string _activateProcessesName;
        private string _startProcessesName;


        #endregion

        public FormMain()
        {
            InitializeComponent();

            paramFile = Thread.GetDomain().BaseDirectory + PARAMFILENAME;

            softwareName = GetInIValStr("softwareName");
            serviceIp = GetInIValStr("ServiceIp");
            port = GetInIValInt("ServicePort");
            ShowItemCount = GetInIValInt("ShowItemCount");

            IsMinToNotifyIcon = GetInIValInt("IsMinToNotifyIcon");

            ThreadSleep = GetInIValInt("ThreadSleepInput");
            if (ThreadSleep < 900)
            {
                ThreadSleep = 1000;
            }
            TextEntrySleep = GetInIValInt("TextEntrySleep");

            panel2.Visible = GetInIValStr("IpPortVisible") == "1" ? true : false;
            isDebug = GetInIValStr("isDebug") == "1" ? true : false;
            pnlTest.Visible = GetInIValStr("pnlTestShow") == "1" ? true : false;

            string version = "";// typeof(FormMain).GetTypeInfo().Assembly.GetCustomAttribute<AssemblyFileVersionAttribute>().Version;
            string buildTime = "";//File.GetLastWriteTime(typeof(FormMain).GetTypeInfo().Assembly.Location).ToString("yyyy-MM-dd HH:mm:ss");

            this.Text = softwareName;

            lblverbuildtime.Text = $"V{version} - {buildTime}";
            if (GetInIValStr("IsShowVerBuildTime") != "1")
            {
                lblverbuildtime.Visible = false;
            }

            // 初始化托盘图标
            if (IsMinToNotifyIcon > 0)
            {
                notifyIcon = new NotifyIcon();
                notifyIcon.Icon = this.Icon; // 使用窗体图标或者指定一个图标
                notifyIcon.Visible = false;
                notifyIcon.Click += new EventHandler(NotifyIcon_Click);
            }


            this.TopMost = GetInIValInt("IsTopMost") > 0 ? true : false;
            this.MinimizeBox = GetInIValInt("IsMinimizeBox") > 0 ? true : false;

            isWirteFile = GetInIValInt("IsCreateNcFile") > 0 ? true : false;
            IsFocusInput = GetInIValInt("IsFocusInput") > 0 ? true : false;
            IsStartProcesses = GetInIValInt("IsStartProcesses") > 0 ? true : false;
            IsSpecifyFocusProcess = GetInIValInt("IsSpecifyFocusProcess") > 0 ? true : false;

            WriteFilePath = GetInIValStr("WriteFilePath");
            _activateProcessesName = GetInIValStr("ActivateProcessesName");
            _startProcessesName = GetInIValStr("StartProcesses");
            _mouseClickX = GetInIValInt("MouseClickX");
            _mouseClickY = GetInIValInt("MouseClickY");

            tcpIpHelper = new PlcTcpIpHelper(serviceIp, port);

            //mouseWatcher = eventHookFactory.GetMouseWatcher();
            //mouseWatcher.OnMouseInput += (s, e) =>
            //{
            //    var mouseEvent = (MouseEventArgs)e.EventArgs;
            //    switch (e.KeyMouseEventType)
            //    {
            //        //case MacroEventType.MouseClick:
            //        case MacroEventType.MouseDown:
            //            //case MacroEventType.MouseUp:
            //            if (mouseEvent.Button == MouseButtons.Middle)
            //            {
            //                ShowMsg("鼠标光标坐标：" + string.Format("x={0:0000}; y={1:0000}", mouseEvent.X, mouseEvent.Y));
            //            }

            //            break;
            //        default:
            //            break;
            //    }
            //};
        }


        private void FormMain_Load(object sender, EventArgs e)
        {
            //if (GetInIValInt("IsMouseWatcher") > 0)
            //{
            //    mouseWatcher.Start(Hook.GlobalEvents());
            //}

            isStart = false;
            txtIp.Text = serviceIp;
            txtPort.Text = port.ToString();

            timer1.Start();

            lblWriteFile.Text = WriteFilePath;

            if (IsFocusInput && string.IsNullOrEmpty(_activateProcessesName))
            {
                MessageBox.Show("未设置焦点输入程序名称，请进行设置再运行！！！", "提示", MessageBoxButtons.OK);
            }
             
        }

        private void btnClientReceive_Click(object sender, EventArgs e)
        {
            if (txtIp.Text.Trim() != serviceIp || txtPort.Text.Trim() != port.ToString())
            {
                IniFile.WriteIni(SECTIONKEY, "ServiceIp", txtIp.Text.Trim(), paramFile);
                IniFile.WriteIni(SECTIONKEY, "ServicePort", txtPort.Text.Trim(), paramFile);

                serviceIp = txtIp.Text.Trim();
                int.TryParse(txtPort.Text.Trim(), out port);
            }
            ShowMsg($"{DateTime.Now:HH:mm:ss} 等待连接启动中 {ThreadSleep} ms");
            Thread.Sleep(ThreadSleep);

            if (isDebug)
            {
                for (int i = 0; i < 20; i++)
                {
                    ParseKeyCode($"788508-{i.ToString("000")}");
                    Thread.Sleep(1000);
                }

                return;
            }

            if (isStart == false)
            {
                ShowMsg($"{DateTime.Now:HH:mm:ss} 开始连接 {txtIp.Text.Trim()}:{txtPort.Text.Trim()}");
                Connect();
            }
            else
            {
                DisconnectTcpIp();
            }
        }

        /// <summary>
        /// 激活进程窗体，显示在最前（或启动进程）
        /// </summary>
        void ActivationSwitchWindow()
        {
            if (IsSpecifyFocusProcess == false) return;

            /*使用Process.Start方法启动程序。
               使用ShowWindow方法设置窗口的状态为最大化。
               使用SetForegroundWindow方法将窗口设置为最前端。*/
            
            //要启动的进程名称，可以在任务管理器里查看，一般是不带.exe后缀的;
            Process[] temp = Process.GetProcessesByName(_activateProcessesName);//在所有已启动的进程中查找需要的进程；
            if (temp.Length > 0)//如果查找到
            {
                IntPtr handle = temp[0].MainWindowHandle;

                SetTargetProgram(handle);
            }
            else if (IsStartProcesses && !string.IsNullOrEmpty(_startProcessesName))
            {
                //Process.Start(_activateProcessesName + ".exe");//否则启动进程
                // 启动外部程序
                Process process = new Process();
                process.StartInfo.FileName = _startProcessesName; // 要启动的程序路径+程序.exe
                process.Start();

                // 等待程序启动
                process.WaitForInputIdle();

                IntPtr handle = process.MainWindowHandle;
                 
                SetTargetProgram(handle);
            }
            else
            {
                ShowMsg($"{DateTime.Now:HH:mm:ss} 焦点输入接收程序未启动，请进行启动！");

                LogHelper.Error("焦点输入未找到要激活接收数据的程序：" + _activateProcessesName);
            }
        }

        void SetTargetProgram(IntPtr handle)
        {
            // 获取当前最前的窗口句柄
            IntPtr foregroundWindowHandle = GetForegroundWindow();

            // 判断当前进程窗口是否是最前 
            if (handle != foregroundWindowHandle)
            {
                SwitchToThisWindow(handle, true); // 激活，显示在最前
            }

            // 将窗口最大化
            ShowWindow(handle, SW_SHOWMAXIMIZED); // SW_MAXIMIZE
             

            if (_mouseClickX > 0 && _mouseClickY > 0)
            {
                // 将鼠标光标移动到指定位置
                SetCursorPos(_mouseClickX, _mouseClickY);

                // 模拟鼠标左键按下和抬起
                mouse_event(MOUSEEVENTF_LEFTDOWN, _mouseClickX, _mouseClickY, 0, 0);
                mouse_event(MOUSEEVENTF_LEFTUP, _mouseClickX, _mouseClickY, 0, 0);
                
            }

        }

        /// <summary>
        /// 焦点输入，或生成对应文件
        /// </summary>
        /// <param name="keyCode"></param>
        void ParseKeyCode(string keyCode)
        {
            if (string.IsNullOrEmpty(keyCode.Trim())|| keyCode.Trim()=="")
            {
                ShowMsg($"{DateTime.Now:HH:mm:ss} 条码为空不执行操作");
                return;
            }

            string fileName = $"{WriteFilePath}\\{keyCode}.nc";
            try
            {
                lblWriteFile.Text = WriteFilePath;
                //优先写NC文件，再进行焦点输入
                if (isWirteFile)
                {
                    File.WriteAllText(fileName, "");
                    LogHelper.Info($"文件 {fileName} 生成完成");
                    ShowMsg($"{DateTime.Now:HH:mm:ss} 文件 {fileName} 生成完成");
                }
                if(IsFocusInput)
                {
                    ActivationSwitchWindow();//激活进程窗体，显示在最前（或启动进程）

                    Thread.Sleep(100);
                    PressCtrlA();
                    Thread.Sleep(100);
                    SimulateKeyPress(VK_BackSpace); // 园方直接删除
                    Thread.Sleep(100);
                    char[] array = keyCode.ToCharArray();
                    int lenght = array.Length;
                    StringBuilder sb = new StringBuilder();
                    for (int i = 0; i < lenght; i++)
                    {
                        byte[] bt = Encoding.ASCII.GetBytes(array[i].ToString());
                        string t="";
                        foreach (var item in bt)
                        {
                            t += item+",";
                        }
                        sb.Append($"{array[i]}:{t} -- ");
                        SimulateKeyPress(bt[0]);
                    }
                     
                    LogHelper.Info($"焦点输入 {keyCode} 完成");
                    ShowMsg($"{DateTime.Now:HH:mm:ss} 焦点输入 {keyCode} 完成");
                }

            }
            catch (Exception e)
            {
                ShowMsg($"{DateTime.Now:HH:mm:ss} {(isWirteFile ? "生成文件" : "焦点输入")} {keyCode} 失败 {e.Message}");
                LogHelper.Error($"焦点输入失败  keyCode:{keyCode}  fileName:{fileName}  isWirteFile:{isWirteFile}  IsFocusInput：{IsFocusInput}", e);
            }

        }

        #region  数据处理
        /// <summary>
        /// 连接扫码枪
        /// </summary>
        public void Connect()
        {
            try
            {
                ScanCameraTcpInit();

                if (scanCameraClient.Connected == false)
                {
                    ShowMsg($"{DateTime.Now:HH:mm:ss} 连接失败");
                }
                else
                {
                    btnClientReceive.Text = "处理中";
                    btnClientReceive.ForeColor = Color.White;
                    btnClientReceive.BackColor = Color.Green;
                    isStart = true;
                    ShowMsg($"{DateTime.Now:HH:mm:ss} 连接成功");
                }
            }
            catch (Exception ex)
            {
                LogHelper.Error($"连接失败", ex);
                ShowMsg($"{DateTime.Now:HH:mm:ss} 连接失败: " + ex.Message);
            }
        }

        /// <summary>
        /// 断开扫码枪
        /// </summary>
        public void DisconnectTcpIp()
        {
            try
            {
                if (scanCameraClient != null)
                {
                    scanCameraClient.Dispose();
                    Thread.Sleep(200);
                    scanCameraClient = null;

                }
                isStart = false;

                btnClientReceive.Text = "开 始";
                btnClientReceive.ForeColor = Color.Black;
                btnClientReceive.BackColor = SystemColors.Control;
                ShowMsg("断开连接-D");
            }
            catch (Exception ex)
            {
                //showMsg("断开连接-D"+ex.Message);
                LogHelper.Error($"断开连接-DE", ex);
            }
        }


        /// <summary>
        /// 连接扫描枪通信
        /// </summary>
        public void ScanCameraTcpInit()
        {
            try
            {
                scanCameraClient = new AsyncTcpClient(IPAddress.Parse(serviceIp), port);
                scanCameraClient.Encoding = Encoding.UTF8;
                scanCameraClient.ServerExceptionOccurred += ScanCameraClient_ServerExceptionOccurred;
                scanCameraClient.ServerConnected += ScanCameraClient_ServerConnected;
                scanCameraClient.ServerDisconnected += ScanCameraClient_ServerDisconnected;
                scanCameraClient.PlaintextReceived += ScanCameraClient_PlaintextReceived4;
                scanCameraClient.Connect();
                Thread.Sleep(millisecondsTimeout: 100);
                if (scanCameraClient.Connected == false)
                {
                    //addOtherHistoryOnBord("连接扫描枪通信失败");
                }
            }
            catch (Exception ex)
            {
                LogHelper.Error($"连接通信失败", ex);
                //LogHelper.Exception(ex);

                //showMsg($"连接扫描枪通信失败-{ex.Message}");
            }
        }
        /// <summary>
        /// 服务异常
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ScanCameraClient_ServerExceptionOccurred(object sender, TcpServerExceptionOccurredEventArgs e)
        {
            LogHelper.Error($"服务异常-ScanCameraClient_ServerExceptionOccurred-{serviceIp}:{port}--{e.Exception.Message}", e.Exception);
            // showMsg("服务异常");
        }
        /// <summary>
        /// 连接成功
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ScanCameraClient_ServerConnected(object sender, TcpServerConnectedEventArgs e)
        {
            LogHelper.Error($"连接成功-ScanCameraClient_ServerConnected-{serviceIp}:{port}-");
            //showMsg("连接成功");
        }
        /// <summary>
        /// 断开连接
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ScanCameraClient_ServerDisconnected(object sender, TcpServerDisconnectedEventArgs e)
        {
            LogHelper.Error($"断开连接-ScanCameraClient_ServerDisconnected-{serviceIp}:{port}--{e.ToString()}");
            //showMsg("断开连接-SD");
        }
        /// <summary>
        /// 接收到的明文
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ScanCameraClient_PlaintextReceived4(object sender, TcpDatagramReceivedEventArgs<string> e)
        {
            string data;
            try
            {
                if (e.Datagram == null)
                {
                    return;
                }
                if (e.Datagram.Trim() == "")
                {
                    return;
                }
                data = e.Datagram.TrimEnd("\r\n".ToCharArray());
                string log = $"接收到 {data}";
                StringBuilder result = new StringBuilder();
                byte[] dataBy = Encoding.UTF8.GetBytes(data);
                foreach (byte val in dataBy)
                {
                    if (val > 33) //ASCII 33之前的值在此无用 32为空格 space
                    {
                        result.Append((char)val);
                    }
                }
                data = result.ToString();
                LogHelper.Info($"{log} 过滤无效字符后：{data}");
                
                ShowMsg($"{DateTime.Now:HH:mm:ss} 接收到 {data}");
                ParseKeyCode(data);
            }
            catch (Exception ex)
            {
                LogHelper.Error($"接收失败", ex);
                ShowMsg($"接收失败{ex}");
            }
        }


        #endregion

        void ShowMsg(string msg)
        {
            if (TotalItemCount > ShowItemCount)
            {
                string text = textBoxLog.Text;
                int indexOfFirstNewLine = text.IndexOf('\n');
                if (indexOfFirstNewLine >= 0)
                {
                    // 删除第一行，包括换行符
                    text = text.Substring(indexOfFirstNewLine + 1);
                    textBoxLog.Text = text;
                }
            }

            textBoxLog.AppendText(msg + "\r\n");
            textBoxLog.ScrollToCaret();

            TotalItemCount++;
        }

        private void FormMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
               
            }
            catch (Exception ex)
            {

            }
        }

        string GetInIValStr(string key)
        {
            return IniFile.ReadIni(SECTIONKEY, key, paramFile);
        }

        int GetInIValInt(string key, int defaultVal = 0)
        {
            int r = 0;
            string str = IniFile.ReadIni(SECTIONKEY, key, paramFile);
            int.TryParse(str, out r);

            if (r == 0)
            {
                return defaultVal;
            }

            return r;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            //启动中，进行检测通信是否断开，如断开则进行重连
            if (isStart == true)
            {
                if (scanCameraClient == null)
                {
                    LogHelper.Error("重新 ScanCameraTcpInit");
                    ScanCameraTcpInit();
                }
                Thread.Sleep(200);

                if (scanCameraClient.Connected == false)
                {
                    scanCameraClient.Connect();
                    LogHelper.Error("重新连接");
                }
            }
            int plcCheckStauts = GetInIValInt("plcCheckStauts", 0);
            //PLC心跳
            if (plcCheckStauts > 0 && tcpIpHelper.PlcChecketStatus(plcCheckStauts) == false)
            {
                LogHelper.Error("PLC心跳检测失败，与PLC通信失败");
            }

        }

        private void btnWriteCode_Click(object sender, EventArgs e)
        {
            Thread.Sleep(1000);
            if (!string.IsNullOrEmpty(txtBarCode.Text.Trim())&& txtBarCode.Text.Trim()!="")
            {
                ParseKeyCode(txtBarCode.Text.Trim());
                txtBarCode.Text = "";
            }
        }

        private void NotifyIcon_Click(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized)
            {
                Show(); // 恢复窗体
                this.WindowState = FormWindowState.Normal; // 将窗体设置为正常状态
                notifyIcon.Visible = false; // 隐藏托盘图标
            } 
        }


        private void FormMain_SizeChanged(object sender, EventArgs e)
        {
            // 初始化托盘图标
            if (IsMinToNotifyIcon > 0)
            {
                if (this.WindowState == FormWindowState.Minimized)
                {
                    Hide(); // 最小化时隐藏窗体
                    notifyIcon.Visible = true; // 显示托盘图标
                }
            }
        }

        private void txtBarCode_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (!string.IsNullOrEmpty(txtBarCode.Text.Trim()) && txtBarCode.Text.Trim()!="")
                {
                    ParseKeyCode(txtBarCode.Text.Trim());
                    txtBarCode.Text = "";
                }
            }
        }

        public void SimulateKeyPress(byte key)
        {
            //焦点输入 jj-A 完成 j:106, -- j:106, -- -:45, -- A:65, -- 
            if ((key >= 65 && key <= 90)||(key > 32 && key < 48) || (key > 57 && key < 65))
            {
                //模拟按下shift键
                keybd_event(vbKeyShift, 0, 0, 0);
            }
            keybd_event(key, 0x45, 0, 0);

            if ((key >= 65 && key <= 90) || (key > 32 && key < 48) || (key > 57 && key < 65))
            {
                //松开按键shift
                keybd_event(vbKeyShift, 0, 2, 0);
            }
            keybd_event(key, 0x45, KEYEVENTF_KEYUP, 0);
        }

        /// <summary>
        /// 模拟按下ctrl+A键
        /// </summary>
        void PressCtrlA()
        {
            //模拟按下ctrl键
            keybd_event(VK_Ctrl, 0, 0, 0);
            //模拟按下A键
            keybd_event(VK_A, 0, 0, 0);
            //松开按键ctrl
            keybd_event(VK_Ctrl, 0, 2, 0);
            //松开按键A
            keybd_event(VK_A, 0, 2, 0);

        }
         
        #region bVk参数 常量定义

        public const byte vbKeyLButton = 0x1;    // 鼠标左键
        public const byte vbKeyRButton = 0x2;    // 鼠标右键
        public const byte vbKeyCancel = 0x3;     // CANCEL 键
        public const byte vbKeyMButton = 0x4;    // 鼠标中键
        public const byte vbKeyBack = 0x8;       // BACKSPACE 键  空格键
        public const byte vbKeyTab = 0x9;        // TAB 键
        public const byte vbKeyClear = 0xC;      // CLEAR 键
        public const byte vbKeyReturn = 0xD;     // ENTER 键
        public const byte vbKeyShift = 0x10;     // SHIFT 键
        public const byte vbKeyControl = 0x11;   // CTRL 键
        public const byte vbKeyAlt = 18;         // Alt 键  (键码18)
        public const byte vbKeyMenu = 0x12;      // MENU 键
        public const byte vbKeyPause = 0x13;     // PAUSE 键
        public const byte vbKeyCapital = 0x14;   // CAPS LOCK 键
        public const byte vbKeyEscape = 0x1B;    // ESC 键
        public const byte vbKeySpace = 0x20;     // SPACEBAR 键
        public const byte vbKeyPageUp = 0x21;    // PAGE UP 键
        public const byte vbKeyEnd = 0x23;       // End 键
        public const byte vbKeyHome = 0x24;      // HOME 键
        public const byte vbKeyLeft = 0x25;      // LEFT ARROW 键
        public const byte vbKeyUp = 0x26;        // UP ARROW 键
        public const byte vbKeyRight = 0x27;     // RIGHT ARROW 键
        public const byte vbKeyDown = 0x28;      // DOWN ARROW 键
        public const byte vbKeySelect = 0x29;    // Select 键
        public const byte vbKeyPrint = 0x2A;     // PRINT SCREEN 键
        public const byte vbKeyExecute = 0x2B;   // EXECUTE 键
        public const byte vbKeySnapshot = 0x2C;  // SNAPSHOT 键
        public const byte vbKeyDelete = 0x2E;    // Delete 键
        public const byte vbKeyHelp = 0x2F;      // HELP 键
        public const byte vbKeyNumlock = 0x90;   // NUM LOCK 键

        //常用键 字母键A到Z
        public const byte vbKeyA = 65;
        public const byte vbKeyB = 66;
        public const byte vbKeyC = 67;
        public const byte vbKeyD = 68;
        public const byte vbKeyE = 69;
        public const byte vbKeyF = 70;
        public const byte vbKeyG = 71;
        public const byte vbKeyH = 72;
        public const byte vbKeyI = 73;
        public const byte vbKeyJ = 74;
        public const byte vbKeyK = 75;
        public const byte vbKeyL = 76;
        public const byte vbKeyM = 77;
        public const byte vbKeyN = 78;
        public const byte vbKeyO = 79;
        public const byte vbKeyP = 80;
        public const byte vbKeyQ = 81;
        public const byte vbKeyR = 82;
        public const byte vbKeyS = 83;
        public const byte vbKeyT = 84;
        public const byte vbKeyU = 85;
        public const byte vbKeyV = 86;
        public const byte vbKeyW = 87;
        public const byte vbKeyX = 88;
        public const byte vbKeyY = 89;
        public const byte vbKeyZ = 90;

        //数字键盘0到9
        public const byte vbKey0 = 48;    // 0 键
        public const byte vbKey1 = 49;    // 1 键
        public const byte vbKey2 = 50;    // 2 键
        public const byte vbKey3 = 51;    // 3 键
        public const byte vbKey4 = 52;    // 4 键
        public const byte vbKey5 = 53;    // 5 键
        public const byte vbKey6 = 54;    // 6 键
        public const byte vbKey7 = 55;    // 7 键
        public const byte vbKey8 = 56;    // 8 键
        public const byte vbKey9 = 57;    // 9 键


        public const byte vbKeyNumpad0 = 0x60;    //0 键
        public const byte vbKeyNumpad1 = 0x61;    //1 键
        public const byte vbKeyNumpad2 = 0x62;    //2 键
        public const byte vbKeyNumpad3 = 0x63;    //3 键
        public const byte vbKeyNumpad4 = 0x64;    //4 键
        public const byte vbKeyNumpad5 = 0x65;    //5 键
        public const byte vbKeyNumpad6 = 0x66;    //6 键
        public const byte vbKeyNumpad7 = 0x67;    //7 键
        public const byte vbKeyNumpad8 = 0x68;    //8 键
        public const byte vbKeyNumpad9 = 0x69;    //9 键
        public const byte vbKeyMultiply = 0x6A;   // MULTIPLICATIONSIGN(*)键
        public const byte vbKeyAdd = 0x6B;        // PLUS SIGN(+) 键
        public const byte vbKeySeparator = 0x6C;  // ENTER 键
        public const byte vbKeySubtract = 0x6D;   // MINUS SIGN(-) 键
        public const byte vbKeyDecimal = 0x6E;    // DECIMAL POINT(.) 键
        public const byte vbKeyDivide = 0x6F;     // DIVISION SIGN(/) 键


        //F1到F12按键
        public const byte vbKeyF1 = 0x70;   //F1 键
        public const byte vbKeyF2 = 0x71;   //F2 键
        public const byte vbKeyF3 = 0x72;   //F3 键
        public const byte vbKeyF4 = 0x73;   //F4 键
        public const byte vbKeyF5 = 0x74;   //F5 键
        public const byte vbKeyF6 = 0x75;   //F6 键
        public const byte vbKeyF7 = 0x76;   //F7 键
        public const byte vbKeyF8 = 0x77;   //F8 键
        public const byte vbKeyF9 = 0x78;   //F9 键
        public const byte vbKeyF10 = 0x79;  //F10 键
        public const byte vbKeyF11 = 0x7A;  //F11 键
        public const byte vbKeyF12 = 0x7B;  //F12 键

        #endregion

    }
 
}
