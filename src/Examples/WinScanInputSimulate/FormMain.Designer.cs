﻿namespace WinScanInputSimulate
{
    partial class FormMain
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.pnlLogShow = new System.Windows.Forms.Panel();
            this.textBoxLog = new System.Windows.Forms.TextBox();
            this.txtIp = new System.Windows.Forms.TextBox();
            this.txtPort = new System.Windows.Forms.TextBox();
            this.btnClientReceive = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.btnWriteCode = new System.Windows.Forms.Button();
            this.txtBarCode = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.lblWriteFile = new System.Windows.Forms.Label();
            this.pnlTest = new System.Windows.Forms.Panel();
            this.lblverbuildtime = new System.Windows.Forms.Label();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.隐藏条码手动输入ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pnlLogShow.SuspendLayout();
            this.panel2.SuspendLayout();
            this.pnlTest.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlLogShow
            // 
            this.pnlLogShow.Controls.Add(this.textBoxLog);
            this.pnlLogShow.Location = new System.Drawing.Point(3, 35);
            this.pnlLogShow.Name = "pnlLogShow";
            this.pnlLogShow.Size = new System.Drawing.Size(292, 316);
            this.pnlLogShow.TabIndex = 22;
            // 
            // textBoxLog
            // 
            this.textBoxLog.BackColor = System.Drawing.Color.White;
            this.textBoxLog.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxLog.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxLog.Location = new System.Drawing.Point(0, 0);
            this.textBoxLog.Multiline = true;
            this.textBoxLog.Name = "textBoxLog";
            this.textBoxLog.ReadOnly = true;
            this.textBoxLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxLog.Size = new System.Drawing.Size(292, 316);
            this.textBoxLog.TabIndex = 11;
            this.textBoxLog.WordWrap = false;
            this.textBoxLog.MouseDown += new System.Windows.Forms.MouseEventHandler(this.textBoxLog_MouseDown);
            // 
            // txtIp
            // 
            this.txtIp.Location = new System.Drawing.Point(19, 7);
            this.txtIp.Name = "txtIp";
            this.txtIp.Size = new System.Drawing.Size(101, 21);
            this.txtIp.TabIndex = 23;
            // 
            // txtPort
            // 
            this.txtPort.Location = new System.Drawing.Point(148, 7);
            this.txtPort.Name = "txtPort";
            this.txtPort.Size = new System.Drawing.Size(40, 21);
            this.txtPort.TabIndex = 23;
            // 
            // btnClientReceive
            // 
            this.btnClientReceive.Location = new System.Drawing.Point(195, 1);
            this.btnClientReceive.Name = "btnClientReceive";
            this.btnClientReceive.Size = new System.Drawing.Size(100, 31);
            this.btnClientReceive.TabIndex = 24;
            this.btnClientReceive.Text = "开 始";
            this.btnClientReceive.UseVisualStyleBackColor = true;
            this.btnClientReceive.Click += new System.EventHandler(this.btnClientReceive_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(2, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(17, 12);
            this.label1.TabIndex = 25;
            this.label1.Text = "IP";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(121, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 12);
            this.label2.TabIndex = 25;
            this.label2.Text = "端口";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.txtPort);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.txtIp);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Location = new System.Drawing.Point(4, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(191, 31);
            this.panel2.TabIndex = 26;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // btnWriteCode
            // 
            this.btnWriteCode.Location = new System.Drawing.Point(197, 3);
            this.btnWriteCode.Name = "btnWriteCode";
            this.btnWriteCode.Size = new System.Drawing.Size(75, 23);
            this.btnWriteCode.TabIndex = 27;
            this.btnWriteCode.Text = "WriteCode";
            this.btnWriteCode.UseVisualStyleBackColor = true;
            this.btnWriteCode.Click += new System.EventHandler(this.btnWriteCode_Click);
            // 
            // txtBarCode
            // 
            this.txtBarCode.Location = new System.Drawing.Point(52, 7);
            this.txtBarCode.Name = "txtBarCode";
            this.txtBarCode.Size = new System.Drawing.Size(139, 21);
            this.txtBarCode.TabIndex = 28;
            this.txtBarCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtBarCode_KeyDown);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 10);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 12);
            this.label3.TabIndex = 29;
            this.label3.Text = "条码";
            // 
            // lblWriteFile
            // 
            this.lblWriteFile.AutoSize = true;
            this.lblWriteFile.Location = new System.Drawing.Point(3, 354);
            this.lblWriteFile.Name = "lblWriteFile";
            this.lblWriteFile.Size = new System.Drawing.Size(41, 12);
            this.lblWriteFile.TabIndex = 30;
            this.lblWriteFile.Text = "label4";
            // 
            // pnlTest
            // 
            this.pnlTest.Controls.Add(this.btnWriteCode);
            this.pnlTest.Controls.Add(this.txtBarCode);
            this.pnlTest.Controls.Add(this.label3);
            this.pnlTest.Location = new System.Drawing.Point(4, 299);
            this.pnlTest.Name = "pnlTest";
            this.pnlTest.Size = new System.Drawing.Size(288, 34);
            this.pnlTest.TabIndex = 31;
            // 
            // lblverbuildtime
            // 
            this.lblverbuildtime.AutoSize = true;
            this.lblverbuildtime.Location = new System.Drawing.Point(21, -1);
            this.lblverbuildtime.Name = "lblverbuildtime";
            this.lblverbuildtime.Size = new System.Drawing.Size(83, 12);
            this.lblverbuildtime.TabIndex = 30;
            this.lblverbuildtime.Text = "ver-buildtime";
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.隐藏条码手动输入ToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(202, 48);
            // 
            // 隐藏条码手动输入ToolStripMenuItem
            // 
            this.隐藏条码手动输入ToolStripMenuItem.Name = "隐藏条码手动输入ToolStripMenuItem";
            this.隐藏条码手动输入ToolStripMenuItem.Size = new System.Drawing.Size(201, 22);
            this.隐藏条码手动输入ToolStripMenuItem.Text = "隐藏/显示条码手动输入";
            this.隐藏条码手动输入ToolStripMenuItem.Click += new System.EventHandler(this.隐藏条码手动输入ToolStripMenuItem_Click);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(299, 373);
            this.Controls.Add(this.lblWriteFile);
            this.Controls.Add(this.pnlTest);
            this.Controls.Add(this.lblverbuildtime);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.btnClientReceive);
            this.Controls.Add(this.pnlLogShow);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FormMain";
            this.Text = "模拟扫码输入";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormMain_FormClosing);
            this.Load += new System.EventHandler(this.FormMain_Load);
            this.SizeChanged += new System.EventHandler(this.FormMain_SizeChanged);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.FormMain_MouseDown);
            this.pnlLogShow.ResumeLayout(false);
            this.pnlLogShow.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.pnlTest.ResumeLayout(false);
            this.pnlTest.PerformLayout();
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Panel pnlLogShow;
        private System.Windows.Forms.TextBox txtIp;
        private System.Windows.Forms.TextBox txtPort;
        private System.Windows.Forms.Button btnClientReceive;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.TextBox textBoxLog;
        private System.Windows.Forms.Button btnWriteCode;
        private System.Windows.Forms.TextBox txtBarCode;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblWriteFile;
        private System.Windows.Forms.Panel pnlTest;
        private System.Windows.Forms.Label lblverbuildtime;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 隐藏条码手动输入ToolStripMenuItem;
    }
}

