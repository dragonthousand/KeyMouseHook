﻿using log4net;
using log4net.Config;
using System;
using System.IO;
using System.Reflection;

namespace WinScanInputSimulate
{
    public class LogHelper
    {
        private static readonly ILog logInfo = LogManager.GetLogger("loginfo");
        static LogHelper()
        {
            var logRepo = LogManager.CreateRepository(Assembly.GetEntryAssembly(), typeof(log4net.Repository.Hierarchy.Hierarchy));
            XmlConfigurator.Configure(logRepo, new FileInfo("log4net.config"));

            log4net.Config.XmlConfigurator.Configure();
        }

        /// <summary>
        /// 记录严重错误日志
        /// </summary>
        /// <param name="message">消息内容</param>
        /// <param name="ex">异常信息</param>
        /// <param name="isWriteDebug">是否在output输出</param>
        public static void Fatal(string message, Exception ex = null, bool isWriteDebug = true)
        {
            logInfo.Fatal(message, ex); 
        }


        /// <summary>
        /// 记录错误日志
        /// </summary>
        /// <param name="message">消息内容</param>
        /// <param name="ex">异常信息</param>
        /// <param name="isWriteDebug">是否在output输出</param>
        public static void Error(string message, Exception ex = null, bool isWriteDebug = true)
        {
            logInfo.Error(message, ex); 
        }


        /// <summary>
        /// 记录警告日志
        /// </summary>
        /// <param name="message">消息内容</param>
        /// <param name="ex">异常信息</param>
        /// <param name="isWriteDebug">是否在output输出</param>
        public static void Warn(string message, Exception ex = null, bool isWriteDebug = true)
        {
            logInfo.Warn(message, ex); 
        }

        /// <summary>
        /// 记录信息日志
        /// </summary>
        /// <param name="message">消息内容</param>
        /// <param name="ex">异常信息</param>
        /// <param name="isWriteDebug">是否在output输出</param>
        public static void Info(string message, Exception ex = null, bool isWriteDebug = true)
        {
            logInfo.Info(message, ex); 
        }

        /// <summary>
        /// 记录调试日志
        /// </summary>
        /// <param name="message">消息内容</param>
        public static void Debug(string message)
        {
            logInfo.Debug(message); 
        }



    }

}