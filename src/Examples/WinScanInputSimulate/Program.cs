﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinScanInputSimulate
{
    internal static class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);


            Process process = Process.GetCurrentProcess();
            Process[] dupl = Process.GetProcessesByName(process.ProcessName);
            if (dupl.Length > 1 )
            {
                MessageBox.Show($"当前应用（{process.ProcessName}）已在运行中！！！", "提示",MessageBoxButtons.OK);

                Environment.Exit(0);//彻底的退出方式，不管什么线程都被强制退出，把程序结束的很干净
            }

            Application.Run(new FormMain());
        }
    }
}
