﻿using System;

public class LogHelper
{
    private static readonly ILog logInfo = LogManager.GetLogger("log4net.config");
    static LogHelper()
    {
        log4net.Config.XmlConfigurator.Configure();
    }

    /// <summary>
    /// 记录严重错误日志
    /// </summary>
    /// <param name="message">消息内容</param>
    /// <param name="ex">异常信息</param>
    /// <param name="isWriteDebug">是否在output输出</param>
    public static void Fatal(string message, Exception ex = null, bool isWriteDebug = true)
    {
        Task.Factory.StartNew(() =>
        {
            logInfo.Fatal(message, ex);
        }).ContinueWith((t) =>
        {
            LogHelper.Error("日志严重错误信息记录错误" + t.Exception.InnerException.GetType().Name);
        },
                TaskContinuationOptions.OnlyOnFaulted);
    }


    /// <summary>
    /// 记录错误日志
    /// </summary>
    /// <param name="message">消息内容</param>
    /// <param name="ex">异常信息</param>
    /// <param name="isWriteDebug">是否在output输出</param>
    public static void Error(string message, Exception ex = null, bool isWriteDebug = true)
    {
        Task.Factory.StartNew(() =>
        {
            logInfo.Error(message, ex);
        }).ContinueWith((t) =>
        {
            LogHelper.Error("日志异常信息记录错误" + t.Exception.InnerException.GetType().Name);
        },
                TaskContinuationOptions.OnlyOnFaulted);
    }


    /// <summary>
    /// 记录警告日志
    /// </summary>
    /// <param name="message">消息内容</param>
    /// <param name="ex">异常信息</param>
    /// <param name="isWriteDebug">是否在output输出</param>
    public static void Warn(string message, Exception ex = null, bool isWriteDebug = true)
    {
        Task.Factory.StartNew(() =>
        {
            logInfo.Warn(message, ex);
        }).ContinueWith((t) =>
        {
            LogHelper.Error("日志警告信息记录错误" + t.Exception.InnerException.GetType().Name);
        },
                TaskContinuationOptions.OnlyOnFaulted);
    }

    /// <summary>
    /// 记录信息日志
    /// </summary>
    /// <param name="message">消息内容</param>
    /// <param name="ex">异常信息</param>
    /// <param name="isWriteDebug">是否在output输出</param>
    public static void Info(string message, Exception ex = null, bool isWriteDebug = true)
    {
        Task.Factory.StartNew(() =>
        {
            logInfo.Info(message, ex);
        }).ContinueWith((t) =>
        {
            LogHelper.Error("日志信息记录错误" + t.Exception.InnerException.GetType().Name);
        },
                TaskContinuationOptions.OnlyOnFaulted);
    }

    /// <summary>
    /// 记录调试日志
    /// </summary>
    /// <param name="message">消息内容</param>
    public static void Debug(string message)
    {
        Task.Factory.StartNew(() =>
        {
            logInfo.Debug(message);
        }).ContinueWith((t) =>
        {
            LogHelper.Error("日志调试信息记录错误" + t.Exception.InnerException.GetType().Name);
        },
        TaskContinuationOptions.OnlyOnFaulted);
    }

    

}