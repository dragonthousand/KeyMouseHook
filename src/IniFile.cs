﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

public class IniFile
{
    [DllImport("kernel32")]
    private static extern long WritePrivateProfileString(string section, string key, string val, string filePath);
    [DllImport("kernel32.dll")]
    private static extern long WritePrivateProfileString(string section, byte[] key, byte[] val, string filePath);

    [DllImport("kernel32")]
    private static extern int GetPrivateProfileString(string section, string key, string def, StringBuilder retVal, int size, string filePath);

    [DllImport("kernel32.dll")]
    private static extern int GetPrivateProfileString(string section, string key, string def, byte[] retVal, int size, string filePath);
     

    [DllImport("kernel32")]
    private static extern uint GetPrivateProfileString(string lpAppName, string lpKeyName, string lpDefault, byte[] lpReturnedString, uint nSize, string lpFileName);

    public static Dictionary<string, string> ReadSectionKeys(string Section, string filePath)
    {
        List<string> Keys = new List<string>();
        byte[] vBuffer = new byte[16384];
        uint vLength = GetPrivateProfileString(Section, null, null, vBuffer, (uint)vBuffer.Length, filePath);
        int j = 0;
        for (int i = 0; i < vLength; i++)
        {
            if (vBuffer[i] == 0)
            {
                Keys.Add(Encoding.Default.GetString(vBuffer, j, i - j));
                j = i + 1;
            }
        }
        Dictionary<string, string> setionValues = new Dictionary<string, string>();
        foreach (string k in Keys)
        {
            StringBuilder temp = new StringBuilder(500);
            GetPrivateProfileString(Section, k, "", temp, 500, filePath);
            string value = temp.ToString();
            setionValues.Add(k, value);
        }
        return setionValues;
    }

    public static void Write(string Section, string Key, string Value, string filePath)
    {
        WriteIni(Section, Key, Value, filePath);
    }


    /// <summary>
    /// 指定节点 指定键写入值
    /// 使用utf-8编码写入 解决乱码问题
    /// </summary>
    /// <param name="Section">节点</param>
    /// <param name="Key">键</param>
    /// <param name="Value">值</param>
    /// <returns></returns>
    public static bool WriteIni(string Section, string Key, string Value, string IniFileName)
    {
        try
        {
            WritePrivateProfileString(Section, Encoding.UTF8.GetBytes(Key), Encoding.UTF8.GetBytes(Value), IniFileName);
            return true;
        }
        catch (Exception ex)
        {
            return false;
        }
    }


    /// <summary>
    /// 读取指定节点 指定键的值
    /// 使用utf-8编码读取 解决乱码问题
    /// </summary>
    /// <param name="Section">节点</param>
    /// <param name="Key">键</param>
    /// <param name="FileName">ini文件</param>
    /// <returns></returns>
    public static string ReadIni(string Section, string Key, string FileName)
    {
        try
        {
            byte[] Buffer = new byte[1024];
            int bufLen = GetPrivateProfileString(Section, Key, "", Buffer, Buffer.GetUpperBound(0), FileName);
            string s = Encoding.UTF8.GetString(Buffer, 0, bufLen);//使用utf-8编码读取 解决乱码问题    
            return s;
        }
        catch (Exception ex)
        {
            return string.Empty;
        }
    }


}

